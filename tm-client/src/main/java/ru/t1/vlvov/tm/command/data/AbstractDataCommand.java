package ru.t1.vlvov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

}
