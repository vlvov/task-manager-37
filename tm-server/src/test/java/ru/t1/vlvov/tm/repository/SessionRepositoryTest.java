package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import java.sql.Connection;

import static ru.t1.vlvov.tm.constant.SessionTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            repository.clear(USER1.getId());
            repository.clear(USER2.getId());
            repository.clear(ADMIN1.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            System.out.println(sessionRepository);
            Assert.assertFalse(sessionRepository.existsById(USER1_SESSION1.getId()));
            System.out.println(USER1_SESSION1);
            sessionRepository.add(USER1_SESSION1);
            connection.commit();
            Assert.assertEquals(USER1_SESSION1.getId(), sessionRepository.findOneById(USER1_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(USER1.getId(), USER1_SESSION1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), sessionRepository.findAll().get(0).getUserId());
            Assert.assertEquals(USER1_SESSION1.getId(), sessionRepository.findAll().get(0).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(USER1_SESSION1);
            sessionRepository.add(USER2_SESSION1);
            connection.commit();
            sessionRepository.clear(USER1.getId());
            connection.commit();
            Assert.assertNull(sessionRepository.findOneById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), sessionRepository.findOneById(USER2_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(SESSION_LIST);
            connection.commit();
            Assert.assertFalse(sessionRepository.findAll(USER1.getId()).isEmpty());
            sessionRepository.clear(USER1.getId());
            connection.commit();
            Assert.assertTrue(sessionRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertFalse(sessionRepository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(USER1_SESSION1);
            sessionRepository.add(USER2_SESSION1);
            connection.commit();
            Assert.assertEquals(USER1_SESSION1.getId(), sessionRepository.findOneById(USER1.getId(), USER1_SESSION1.getId()).getId());
            Assert.assertNull(sessionRepository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(USER1_SESSION1);
            sessionRepository.add(USER2_SESSION1);
            connection.commit();
            sessionRepository.remove(USER1.getId(), USER1_SESSION1);
            connection.commit();
            Assert.assertNull(sessionRepository.findOneById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), sessionRepository.findOneById(USER2_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(USER1_SESSION1);
            sessionRepository.add(USER2_SESSION1);
            connection.commit();
            sessionRepository.removeById(USER1.getId(), USER1_SESSION1.getId());
            connection.commit();
            Assert.assertNull(sessionRepository.findOneById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), sessionRepository.findOneById(USER2_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(USER1_SESSION1);
            connection.commit();
            Assert.assertTrue(sessionRepository.existsById(USER1_SESSION1.getId()));
            Assert.assertFalse(sessionRepository.existsById(USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
