package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import java.sql.Connection;

import static ru.t1.vlvov.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vlvov.tm.constant.TaskTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.clear(USER1.getId());
            repository.clear(USER2.getId());
            repository.clear(ADMIN1.getId());
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            Assert.assertFalse(taskRepository.existsById(USER1_TASK1.getId()));
            taskRepository.add(USER1_TASK1);
            connection.commit();
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1.getId(), USER1_TASK1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), taskRepository.findAll().get(0).getUserId());
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findAll().get(0).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            connection.commit();
            taskRepository.clear(USER1.getId());
            connection.commit();
            Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(TASK_LIST);
            connection.commit();
            Assert.assertFalse(taskRepository.findAll(USER1.getId()).isEmpty());
            taskRepository.clear(USER1.getId());
            connection.commit();
            Assert.assertTrue(taskRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertFalse(taskRepository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            connection.commit();
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
            Assert.assertNull(taskRepository.findOneById(USER1.getId(), USER2_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            connection.commit();
            taskRepository.remove(USER1.getId(), USER1_TASK1);
            connection.commit();
            Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            connection.commit();
            taskRepository.removeById(USER1.getId(), USER1_TASK1.getId());
            connection.commit();
            Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1_TASK1);
            connection.commit();
            Assert.assertTrue(taskRepository.existsById(USER1_TASK1.getId()));
            Assert.assertFalse(taskRepository.existsById(USER2_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByProjectIdByUserId() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(USER1_TASK_LIST);
            taskRepository.add(USER2_TASK_LIST);
            connection.commit();
            Assert.assertFalse(taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
