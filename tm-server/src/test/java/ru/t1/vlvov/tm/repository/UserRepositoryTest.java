package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.IUserRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import java.sql.Connection;

import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(propertyService, connection);
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER1);
            repository.add(USER2);
            connection.commit();
            repository.clear();
            connection.commit();
            Assert.assertNull(repository.findOneById(USER1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER_LIST);
            connection.commit();
            Assert.assertFalse(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
            repository.remove(USER1);
            connection.commit();
            Assert.assertNull(repository.findOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER1);
            connection.commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
            repository.removeById(USER1.getId());
            connection.commit();
            Assert.assertNull(repository.findOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(USER1);
            connection.commit();
            Assert.assertTrue(repository.existsById(USER1.getId()));
            repository.removeById(USER1.getId());
            connection.commit();
            Assert.assertFalse(repository.existsById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPassword() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            Assert.assertFalse(repository.isLoginExist("USER1_LOGIN"));
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD");
            connection.commit();
            Assert.assertTrue(repository.isLoginExist("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPasswordEmail() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            Assert.assertFalse(repository.isLoginExist("USER1_LOGIN"));
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD", "USER1_EMAIL");
            connection.commit();
            Assert.assertTrue(repository.isLoginExist("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPasswordRole() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            Assert.assertFalse(repository.isLoginExist("USER1_LOGIN"));
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD", Role.ADMIN);
            connection.commit();
            Assert.assertTrue(repository.isLoginExist("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD");
            connection.commit();
            Assert.assertEquals(user.getId(), repository.findByLogin(user.getLogin()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD", "EMAIL_TEST");
            connection.commit();
            Assert.assertEquals(user.getId(), repository.findByEmail(user.getEmail()).getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void isLoginExist() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD");
            connection.commit();
            Assert.assertTrue(repository.isLoginExist(user.getLogin()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    @SneakyThrows
    public void isEmailExist() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull User user = repository.create("USER1_LOGIN", "USER1_PASSWORD", "TEST_EMAIL");
            connection.commit();
            Assert.assertTrue(repository.isEmailExist(user.getEmail()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
