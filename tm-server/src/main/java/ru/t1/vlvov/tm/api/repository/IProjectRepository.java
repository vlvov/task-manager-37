package ru.t1.vlvov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    Project update(@NotNull Project model);

}
