package ru.t1.vlvov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IProjectTaskService;
import ru.t1.vlvov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.vlvov.tm.exception.entity.TaskNotFoundException;
import ru.t1.vlvov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.vlvov.tm.exception.field.TaskIdEmptyException;
import ru.t1.vlvov.tm.exception.field.UserIdEmptyException;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    public ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();

        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();


        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final Task task : tasks) taskRepository.remove(userId, task);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
