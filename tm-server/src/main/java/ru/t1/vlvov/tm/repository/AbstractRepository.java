package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IRepository;
import ru.t1.vlvov.tm.comparator.CreatedComparator;
import ru.t1.vlvov.tm.comparator.StatusComparator;
import ru.t1.vlvov.tm.constant.FieldConstant;
import ru.t1.vlvov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @NotNull
    protected final Connection connection;

    @NotNull
    protected final String table;

    public AbstractRepository(@NotNull final Connection connection, @NotNull final String table) {
        this.connection = connection;
        this.table = table;
    }

    @NotNull
    protected String getTable() {
        return table;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return FieldConstant.CREATED;
        if (comparator == StatusComparator.INSTANCE) return FieldConstant.STATUS;
        return FieldConstant.NAME;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet rowSet);

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @SneakyThrows
    @Override
    public void clear() {
        @NotNull final String sql = String.format("TRUNCATE TABLE %s", getTable());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTable());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) {
                result.add(fetch(rowSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE ID = ? LIMIT 1", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) return fetch(rowSet);
            return null;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE ID = ?", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s", getTable(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) {
                result.add(fetch(rowSet));
            }
        }
        return result;
    }

}