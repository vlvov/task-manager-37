package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.model.User;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String login(@Nullable final String login, @Nullable final String password);

    @NotNull
    Session validateToken(@Nullable final String token);

    @NotNull
    void logout(@Nullable final String token);

}
