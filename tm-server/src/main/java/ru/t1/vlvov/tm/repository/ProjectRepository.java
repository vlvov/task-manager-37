package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.constant.FieldConstant;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection, "PROJECT");
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull Project model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID)"
                        + "VALUES (?, ?, ?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, Status.NOT_STARTED.getDisplayName());
            statement.setString(6, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull String userId, @NotNull Project model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Project project = new Project();
        project.setId(rowSet.getString(FieldConstant.ID));
        project.setName(rowSet.getString(FieldConstant.NAME));
        project.setDescription(rowSet.getString(FieldConstant.DESCRIPTION));
        project.setUserId(rowSet.getString(FieldConstant.USER_ID));
        project.setStatus(Status.toStatus(rowSet.getString(FieldConstant.STATUS)));
        project.setCreated(rowSet.getTimestamp(FieldConstant.CREATED));
        return project;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project update(@NotNull Project model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET NAME = ?, DESCRIPTION = ?, STATUS = ? WHERE ID = ?", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().getDisplayName());
            statement.setString(4, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}
