package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.constant.FieldConstant;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection, "TASK");
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull Task model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID, PROJECT_ID)"
                        + "VALUES (?, ?, ?, ?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, Status.NOT_STARTED.getDisplayName());
            statement.setString(6, model.getUserId());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull String userId, @NotNull Task model) {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Task task = new Task();
        task.setId(rowSet.getString(FieldConstant.ID));
        task.setName(rowSet.getString(FieldConstant.NAME));
        task.setDescription(rowSet.getString(FieldConstant.DESCRIPTION));
        task.setUserId(rowSet.getString(FieldConstant.USER_ID));
        task.setStatus(Status.toStatus(rowSet.getString(FieldConstant.STATUS)));
        task.setCreated(rowSet.getTimestamp(FieldConstant.CREATED));
        task.setProjectId(rowSet.getString(FieldConstant.PROJECT_ID));
        return task;
    }

    @NotNull
    @SneakyThrows
    public Task update(@NotNull Task model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET NAME = ?, DESCRIPTION = ?, STATUS = ?, PROJECT_ID = ? WHERE ID = ?", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().getDisplayName());
            statement.setString(4, model.getProjectId());
            statement.setString(5, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE PROJECT_ID = ?", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) {
                result.add(fetch(rowSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE PROJECT_ID = ? AND USER_ID = ?", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) {
                result.add(fetch(rowSet));
            }
        }
        return result;
    }

}
