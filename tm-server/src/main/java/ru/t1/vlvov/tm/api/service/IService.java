package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.api.repository.IRepository;
import ru.t1.vlvov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
