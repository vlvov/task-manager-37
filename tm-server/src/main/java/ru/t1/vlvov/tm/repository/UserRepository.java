package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IUserRepository;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.constant.FieldConstant;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.service.PropertyService;
import ru.t1.vlvov.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final IPropertyService propertyService;

    public UserRepository(@NotNull final IPropertyService propertyService, @NotNull final Connection connection) {
        super(connection, "vlvov.USER");
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD, ROLE, LOCKED)"
                        + "VALUES (?, ?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, login);
            statement.setString(3, HashUtil.salt(password, new PropertyService()));
            statement.setString(4, Role.USUAL.getDisplayName());
            statement.setBoolean(5, false);
            statement.executeUpdate();
        }
        return findByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD, ROLE, LOCKED, EMAIL)"
                        + "VALUES (?, ?, ?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, login);
            statement.setString(3, HashUtil.salt(password, new PropertyService()));
            statement.setString(4, Role.USUAL.getDisplayName());
            statement.setBoolean(5, false);
            statement.setString(6, email);
            statement.executeUpdate();
        }
        return findByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD, ROLE, LOCKED)"
                        + "VALUES (?, ?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, login);
            statement.setString(3, HashUtil.salt(password, new PropertyService()));
            statement.setString(4, role.getDisplayName());
            statement.setBoolean(5, false);
            statement.executeUpdate();
        }
        return findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE LOGIN = ? LIMIT 1", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) return fetch(rowSet);
            return null;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE EMAIL = ? LIMIT 1", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) return fetch(rowSet);
            return null;
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@NotNull final String login) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE LOGIN = ? LIMIT 1", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.next();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@NotNull final String email) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE EMAIL = ? LIMIT 1", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            return rowSet.next();
        }
    }

    @Override
    @SneakyThrows
    protected @NotNull User fetch(@NotNull ResultSet rowSet) {
        @NotNull final User user = new User();
        user.setId(rowSet.getString(FieldConstant.ID));
        user.setLogin(rowSet.getString(FieldConstant.LOGIN));
        user.setPasswordHash(rowSet.getString(FieldConstant.PASSWORD));
        user.setFirstName(rowSet.getString(FieldConstant.FIRST_NAME));
        user.setLastName(rowSet.getString(FieldConstant.LAST_NAME));
        user.setMiddleName(rowSet.getString(FieldConstant.MIDDLE_NAME));
        user.setEmail(rowSet.getString(FieldConstant.EMAIL));
        user.setRole(Role.toRole(rowSet.getString(FieldConstant.ROLE)));
        user.setLocked(rowSet.getBoolean(FieldConstant.LOCKED));
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User add(@NotNull User model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (ID, LOGIN, PASSWORD, FIRST_NAME, LAST_NAME, MIDDLE_NAME, EMAIL, ROLE, LOCKED)"
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getFirstName());
            statement.setString(5, model.getLastName());
            statement.setString(6, model.getMiddleName());
            statement.setString(7, model.getEmail());
            statement.setString(8, model.getRole().getDisplayName());
            statement.setBoolean(9, model.getLocked());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@NotNull final String userId, @NotNull final String password) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET PASSWORD = ? WHERE ID = ?", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, HashUtil.salt(password, new PropertyService()));
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return findOneById(userId);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User updateUser(@NotNull final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET FIRST_NAME = ?, LAST_NAME = ?, MIDDLE_NAME = ? WHERE ID = ?", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, middleName);
            statement.setString(4, id);
            statement.executeUpdate();
        }
        return findOneById(id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User lockUser(@NotNull final String id) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET LOCKED = TRUE WHERE ID = ?", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
        return findOneById(id);
    }

    @Nullable
    @SneakyThrows
    @Override
    public User unlockUser(@NotNull final String id) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET LOCKED = FALSE WHERE ID = ?", getTable()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
        return findOneById(id);
    }

}
