package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
