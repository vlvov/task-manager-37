package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.IUserOwnedRepository;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection, @NotNull final String table) {
        super(connection, table);
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model);

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE USER_ID = ?", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) {
                result.add(fetch(rowSet));
            }
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final String sql = String.format("DELETE FROM %s WHERE USER_ID = ?", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE ID = ? AND USER_ID = ? LIMIT 1", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) return fetch(rowSet);
            return null;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@NotNull final String userId, @NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE ID = ? AND USER_ID = ?", getTable());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE USER_ID = ? ORDER BY %s", getTable(), getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery(sql);
            while (rowSet.next()) {
                result.add(fetch(rowSet));
            }
        }
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

}
