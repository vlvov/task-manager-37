package ru.t1.vlvov.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

}
