package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.api.repository.IUserOwnedRepository;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
