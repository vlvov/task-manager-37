package ru.t1.vlvov.tm.dto.Response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.model.User;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(User user) {
        super(user);
    }

}
